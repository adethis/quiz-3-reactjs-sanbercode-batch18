import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
// Pages
import { Home, About, Movies, Login } from './pages'
// Utils
import  { AuthProvider } from './context/AuthContext'
// Components
import { Header, GuardRoute } from './components'
// Assets
import './assets/css/style.css'

function App() {
  return (
    <>
      <AuthProvider>
        <Router>
          <Header />
          <Switch>
            <Route path='/about'>
              <About />
            </Route>
            <GuardRoute path='/movies'>
              <Movies />
            </GuardRoute>
            <Route path='/login'>
              <Login />
            </Route>
            <Route path='/'>
              <Home />
            </Route>
          </Switch>
        </Router>
      </AuthProvider>
    </>
  );
}

export default App
