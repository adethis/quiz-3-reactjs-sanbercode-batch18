import React, { useState, createContext } from 'react'

export const AuthContext = createContext()

const session = () => {
  let auth = localStorage.getItem('session') === null ? false : true
  return auth
}

export const AuthProvider = props => {
  const [ isLogin, setLogin ] = useState(session)

  const setSession = params => {
    if (params === true) {
      localStorage.setItem('session', true)
      setLogin(true)
    } else {
      localStorage.removeItem('session')
      setLogin(false)
    }
  }

  return (
    <AuthContext.Provider value={[isLogin, setSession]}>
      {props.children}
    </AuthContext.Provider>
  )
}