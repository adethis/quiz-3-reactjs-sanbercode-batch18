import React, { useState, useEffect, createContext } from 'react'
import { getMovies } from '../utils/MoviesAPI'

export const MoviesContext = createContext()

export const MoviesProvider = props => {
  const input = {
    title: '',
    description: '',
    year: 2020,
    duration: 120,
    genre: '',
    rating: 0,
    image_url: '',
    id: null
}
  const [inputMovie, setInputMovie] = useState(input)
  const [dataMovies, setDataMovies] = useState(null)

  const fetchData = () => {
    return (
      getMovies()
        .then(res => {
          setDataMovies(res.data)
        })
    )
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <MoviesContext.Provider
      value={[ 
        inputMovie, 
        setInputMovie, 
        dataMovies, 
        setDataMovies, 
        fetchData,
        input ]}>
      {props.children}
    </MoviesContext.Provider>
  )

}