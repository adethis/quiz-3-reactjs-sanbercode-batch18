import React, { Component } from 'react'
import { getMovies } from '../utils/MoviesAPI'
import { Card, Section } from '../components'

class Home extends Component {
  constructor() {
    super()
    this.state = {
      dataMovies: null
    }
  }

  componentDidMount() {
    getMovies()
      .then(res => {
        this.setState({dataMovies: res.data})
      })
  }

  render() {
    return (
      <>
        <Section>
          <h1 className='text-center'>Daftar Film Terbaik</h1>
          {
            this.state.dataMovies !== null &&
            this.state.dataMovies.map((value, index) => {
              return (
                <Card className='self with-thumb' key={index}>
                  <div className='card-header'>
                    <h1>{value.title}</h1>
                  </div>
                  <div className='card-body'>
                    <img src={value.image_url} alt={value.title} />
                    <div>
                      <ul className='list-detail'>
                        <li>Rating: {value.rating}</li>
                        <li>Duration: {value.duration}</li>
                        <li>Genre: {value.genre}</li>
                      </ul>
                      <p>{value.description}</p>
                    </div>
                  </div>
                </Card>
              )
            })
          }
        </Section>
      </>
    )
  }
}

export default Home