import Home from './Home'
import About from './About'
import Movies from './Movies'
import Login from './Login'

export { Home, About, Movies, Login }