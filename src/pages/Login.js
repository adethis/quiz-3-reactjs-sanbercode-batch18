import React, { useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'

// Component
import { Section, Card, Input, Button } from '../components'

const Login = () => {
  const [isLogin, setLogin] = useContext(AuthContext)
  const defaultInput = { username: '', password: '' }
  const [input, setInput] = useState(defaultInput)

  const handleChange = evt => {
    setInput({ ...input, [evt.target.name]: evt.target.value })
  }

  const handleSubmit = evt => {
    evt.preventDefault()
    if (!input.name && !input.password) {
      alert('Please provide form input')
    } else {
      setLogin(true)
    }
  }

  return (
    <>
      {
        isLogin && <Redirect to='/' />
      }
      <Section>
        <h1 className='text-center'>Login</h1>
        <Card>
          <form onSubmit={handleSubmit}>
            <table cellSpacing='12'>
              <tbody>
                <tr>
                  <td>Username</td>
                  <td></td>
                  <td>
                    <Input
                      type='text'
                      name='username'
                      value={input.username}
                      onChange={handleChange}
                      placeholder='Username' />
                  </td>
                </tr>
                <tr>
                  <td>Password</td>
                  <td></td>
                  <td>
                    <Input 
                      type='password' 
                      name='password' 
                      value={input.password}
                      onChange={handleChange}
                      placeholder='Password' />
                  </td>
                </tr>
                <tr>
                  <td><Button className='btn btn-primary' type='submit' text='Login' /></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </form>
        </Card>
      </Section>
    </>
  )
}

export default Login