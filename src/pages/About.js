import React from 'react'
import { Link } from 'react-router-dom'

// Component
import { Card, Section } from '../components'

const About = () => {
  return (
    <>
      <Section>
        <h1 className='text-center'>Data Peserta Sanbercode Bootcamp ReactJS</h1>
        <Card>
          <ul>
            <li>
              <strong>1. Nama : </strong> Ade Trihadi Setiawan
            </li>
            <li>
              <strong>2. Email : </strong> adet91@gmail.com
            </li>
            <li>
              <strong>3. Sistem Operasi : </strong> Mac OS/OSX, Windows
            </li>
            <li>
              <strong>4. Gitlab : </strong> adethis
            </li>
            <li>
              <strong>5. Gitlab : </strong> @hellhollow
            </li>
          </ul>
          <br />
          <Link to='/'>Kembali</Link>
        </Card>
      </Section>
    </>
  );
}

export default About