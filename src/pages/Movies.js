import React, { useState } from 'react'
import { useEffect } from 'react'
import { Section, Card, Input, Button } from '../components'
import { getMovies, postMovies, deleteMovies, putMovies } from '../utils/MoviesAPI'

const Movies = () => {
  const input = {
    title: '',
    description: '',
    year: 2020,
    duration: 120,
    genre: '',
    rating: 0,
    image_url: '',
    id: null
  }
  const [inputMovie, setInputMovie] = useState(input)
  const [dataMovies, setDataMovies] = useState(null)
  const [search, setSearch] = useState('')

  const fetchData = () =>
  getMovies()
    .then(res => {
      setDataMovies(res.data)
    })

  useEffect(() => {
    fetchData()
  }, [])

  const handleChange = evt => {
    let name = evt.target.name
    let value = evt.target.value
    setInputMovie({...inputMovie, [name]: value})
  }

  const handleEdit = evt => {
    let id = parseInt(evt.target.value)
    let movie = dataMovies.find(d => d.id === id)
    setInputMovie(movie)
  }

  const handleDelete = evt => {
    const id = evt.target.value
    if (id !== null && id !== '') {
      deleteMovies(id)
        .then(res => {
          let data = dataMovies.filter(d => d.id !== id)
          setDataMovies(data)
          fetchData()
        })
    }
  }

  const handleSubmit = evt => {
    evt.preventDefault()
    if (inputMovie.id === null) {
      postMovies({...inputMovie})
        .then(res => {
          setDataMovies([...dataMovies, res.data])
          setInputMovie(input)
          fetchData(setDataMovies)
        })
        .catch(error => console.log(error))
    } else {
      putMovies({...inputMovie})
        .then(res => {
          let data = dataMovies.map(item => {
            if (item.id === inputMovie.id) {
              item.title = inputMovie.title
              item.description = inputMovie.description
              item.year = inputMovie.year
              item.duration = inputMovie.duration
              item.genre = inputMovie.genre
              item.rating = inputMovie.rating
              item.image_url = inputMovie.image_url
            }
            return item
          })
          setDataMovies(data)
          setInputMovie(input)
          fetchData()
        })
    }
  }
  
  const handleSearch = evt => {
    let value = evt.target.value
    setSearch(value)
  }

  const handleSubmitSearch = evt => {
    evt.preventDefault()
    if (search !== '') {
      let result = dataMovies.filter(d => {
        return d.title.toLowerCase().includes(search.toLowerCase())
      })
      setDataMovies(result)
    } else {
      fetchData(setDataMovies)
    }
  }

  const handleLimit = evt => {
    if (evt !== null && evt !== undefined)
      if (evt.length > 10)
        return `${evt.substr(0, 30)}...`
      else return evt
    else return evt
  }

  return (
    <Section>
      <h1 className='text-center'>Movie List Editor</h1>
      <Card className='self'>
        <form onSubmit={handleSubmitSearch}>
          <div className='form-group text-center'>
            <Input type='text' name='search' onChange={handleSearch} placeholder='Search' />
            &nbsp;
            <Button className='btn btn-primary' text='Search' />
          </div>
        </form>

        <h3>Daftar Film</h3>
        <table cellPadding='4' width='100%' className='with-border'>
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Description</th>
              <th>Year</th>
              <th>Duration</th>
              <th>Genre</th>
              <th>Rating</th>
              <th colSpan='2' className='text-center'>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              dataMovies !== null &&
              dataMovies.map((value, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{handleLimit(value.title)}</td>
                    <td>{handleLimit(value.description)}</td>
                    <td>{value.year}</td>
                    <td>{value.duration}</td>
                    <td>{value.genre}</td>
                    <td>{value.rating}</td>
                    <td className='text-center'>
                      <Button className='btn btn-edit btn-sm' text='Edit' value={value.id} onClick={handleEdit} />
                    </td>
                    <td className='text-center'>
                      <Button className='btn btn-delete btn-sm' text='Delete' value={value.id} onClick={handleDelete} />
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
        <br />
        <h3>Movies Form</h3>
        <form onSubmit={handleSubmit}>
          <div className='form-group-flex'>
            <label>Title</label>
            <Input type='text' name='title' value={inputMovie.title} onChange={handleChange} placeholder='Title' />
          </div>
          <div className='form-group-flex'>
            <label>Description</label>
            <textarea name='description' value={inputMovie.description} onChange={handleChange} placeholder='Description' rows='5'></textarea>
          </div>
          <div className='form-group-flex'>
            <label>Year</label>
            <Input type='number' name='year' min='1980' value={inputMovie.year} onChange={handleChange} />
          </div>
          <div className='form-group-flex'>
            <label>Duration</label>
            <Input type='number' name='duration' value={inputMovie.duration} onChange={handleChange} />
          </div>
          <div className='form-group-flex'>
            <label>Genre</label>
            <Input type='text' name='genre' value={inputMovie.genre} onChange={handleChange} placeholder='Genre' />
          </div>
          <div className='form-group-flex'>
            <label>Rating</label>
            <Input type='number' name='rating' min='0' value={inputMovie.rating} onChange={handleChange} placeholder='Rating' />
          </div>
          <div className='form-group-flex'>
            <label>Image URL</label>
            <textarea name='image_url' rows='4' placeholder='Image URL' value={inputMovie.image_url} onChange={handleChange}></textarea>
          </div>
          <div className='form-group-flex'>
            <Button className='btn btn-primary' text='Submit' />
          </div>
        </form>
      </Card>
    </Section>
  )
}

export default Movies