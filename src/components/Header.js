import React from 'react'
import { useContext } from 'react'
import { NavLink } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'

// Assets
import logo from '../assets/img/logo.png'

const Header = () => {
  const [ isLogin, setSession ] = useContext(AuthContext)
  return (
    <header className='navbar'>
      <nav>
        <div className='logo'>
          <img src={logo} alt='logo' />
        </div>
        <ul>
          <li>
            <NavLink to='/' activeClassName='active' exact>Home</NavLink>
          </li>
          <li>
            <NavLink to='/about' activeClassName='active'>About</NavLink>
          </li>
          {
            isLogin ?
            <>
              <li>
                <NavLink to='/movies' activeClassName='active'>Movie List Editor</NavLink>
              </li>
              <li>
                <NavLink to='/' onClick={(e) => {
                  e.preventDefault()
                  setSession(false)
                }}>Log Out</NavLink>
              </li>
            </>
            :
            <li>
              <NavLink to='/login' activeClassName='active'>Login</NavLink>
            </li>
          }
        </ul>
      </nav>
    </header>
  )
}

export default Header