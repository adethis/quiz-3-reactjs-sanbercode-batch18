import React from 'react'

const Input = (props) => {
  return (
    <>
      <input 
        type={props.type}
        name={props.name}
        value={props.value}
        min={props.min}
        onChange={props.onChange}
        placeholder={props.placeholder} />
    </>
  )
}

export default Input