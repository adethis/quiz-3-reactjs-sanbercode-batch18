import React from 'react'
import { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'

const GuardRoute = ({children, ...rest}) => {
  const [isLogin] = useContext(AuthContext)
  return (
    <Route {...rest}>
      {isLogin ? children : <Redirect to='/login' />}
    </Route>
  )
}

export default GuardRoute