import React from 'react'

const Section = (props) => {
  return (
    <section className='content'>
      {props.children}
    </section>
  )
}

export default Section