import axios from 'axios'

// Path API
// backendexample.sanbercloud.com

export const getMovies = () => axios.get('http://backendexample.sanbercloud.com/api/movies')

export const postMovies = params => axios.post('http://backendexample.sanbercloud.com/api/movies', (params))

export const deleteMovies = params => axios.delete(`http://backendexample.sanbercloud.com/api/movies/${params}`)

export const putMovies = params => axios.put(`http://backendexample.sanbercloud.com/api/movies/${params.id}`, (params))